$(document).ready(function() {
    $('.list-group-item.quote').click(function() {
        $(".hidden_radio_quote").attr("checked", false);
        $("#quote" + $(this).data('id')).prop("checked", true);
        $('.list-group-item.quote').removeClass('active');
        $('#quote' + $(this).data('id') + 'cover').addClass('active');
    });
    $('.list-group-item.image').click(function() {
        $(".hidden_radio_image").attr("checked", false);
        $("#image" + $(this).data('id')).prop("checked", true);
        $('.list-group-item.image').removeClass('active');
        $('#image' + $(this).data('id') + 'cover').addClass('active');
    });

    $("#show_list_index_filter").click(function() {
        if($("#list_index_filter").css("display") == 'none') {
            $("#list_index_filter").show();
            $(this).text('Спрятать фильтр');
        } else {
            $("#list_index_filter").hide();
            $(this).text('Показать фильтр');
        }
    });

    $("#show_preview").click(function() {
        var query = [],
            name,
            value,
            uri = 'http://citata.guru/card/';
        if($(".hidden_radio_image:checked").size()) {
            uri += $(".hidden_radio_image:checked").val();
            if($(".hidden_radio_quote:checked").size()) {
                query.push($(".hidden_radio_quote:checked").attr('name') + "=" + $(".hidden_radio_quote:checked").val());
            }
            $(".form-control").each(function() {
                value = $(this).val();
                name = $(this).attr('name');
                if (name && value) {
                    query.push(name+ "=" + value);
                }
            });
            uri += "?" + query.join("&");
            $("#preview").html(
                '<iframe class="iframe_scale" width="1500" height="1500" src="' + uri + '"></iframe>'
            );
        }
    });

    if($("#bottom_marker").size()) {
        var page = 1;
        var tag = $("#bottom_marker").data('tag');
        page = moarcallback(page, tag, 7);
        $(window).scroll(function() { //detect page scroll
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                page = moar(page, tag);
            }
        });
    }
});

var callbacks_called = 0;
function moarcallback(page, tag, max) {
    $.get("/block?page=" + page + "&tag=" + tag, function(data){
        $(data).insertBefore("#bottom_marker");
        callbacks_called++;
        if (callbacks_called < max) {
            page++;
            moarcallback(page, tag, max);
        }
    });
    return page+max;
}

function moar(page, tag, callback) {
    $.get("/block?page=" + page + "&tag=" + tag, function(data){
        $(data).insertBefore("#bottom_marker");
    });
    return ++page;
}
<?php namespace App;

use Aws\CloudFront\Exception\Exception;
use Illuminate\Database\Eloquent\Model;
use VK\VK;
use Config;
use Storage;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use App\Quote;
use DB;
use App\Tumblr;

class Card extends Model
{

    public function quote() {
        return $this->hasOne('App\Quote');
    }

    public function getTags() {
        $selected_tags = [];
        $tumblr_tags = DB::table('tags')
            ->join('tag_relation', 'tags.id', '=', 'tag_relation.tag_id')
            ->where('tag_relation.entity_code', '=', 'card')
            ->where('tag_relation.entity_id', '=', $this->id)
            ->select('tags.id','tags.title')
            ->get();
        foreach ($tumblr_tags as $tag) {
            $selected_tags[$tag->id] = $tag->title;
        }
        return $selected_tags;
    }

    public function getPanel() {
        $panels = [
            'panel-default',
//            'panel-primary',
            'panel-success',
            'panel-info',
            'panel-warning',
//            'panel-danger',
        ];
        return $panels[rand(0, count($panels) - 1)];
    }

    public function getQuote() {
        $quote = Quote::find($this->quote);
        return $quote ? $quote->title : '';
    }

    public function getNext() {
        $card = Card::where('id', '>', $this->id)->orderBy('id', 'asc')->first();
        if (!$card) {
            $card = Card::get()->first();
        }
        return $card->id;
    }

    public function getPrev() {
        $card = Card::where('id', '<', $this->id)->orderBy('id', 'desc')->first();
        if (!$card) {
            $card = Card::get()->first();
        }
        return $card->id;
    }

    public static function createRandomCard() {
        $quote = Quote::where("approved", "=", "1")->where("disabled", "!=", "1")->where("used", "!=", "1")->orderByRaw("RAND()")->first();
        $image = Image::where("approved", "=", "1")->where("disabled", "!=", "1")->where("used", "!=", "1")->orderByRaw("RAND()")->first();
        if ($quote && $image) {
            $latest_card = Card::where('published', '=', '0')->orderBy('publish_at', 'desc')->first();
            if (!$latest_card) {
                $time = time() + Config::get("vk.posting_period") + rand(0, Config::get("vk.posting_spread"));
            } else {
                $time = strtotime($latest_card->publish_at);
                if ($time < time()) {
                    $time = time() + Config::get("vk.posting_period") + rand(0, Config::get("vk.posting_spread"));
                }
            }
            self::addToSchedule($image->id, $quote->id, date("Y-m-d H:i:s", strtotime($time)));
        }
    }

    public static function setOfTheDay() {
        $next = Card::where('approved', '=', '1')->where('oftheday', '=', '0')->orderByRaw("RAND()")->first();
        if ($current = Card::where('oftheday', '=', '1')->first()) {
            $current->oftheday = 0;
            $current->save();
        }
        $next->oftheday = 1;
        $next->save();
    }

    public static function createCard($image, $quote) {
        //удаляем старую временную открытку
        if (Storage::exists('card.jpg')) {
            Storage::delete('card.jpg');
        }
        //отмечаем цитату и картинку как использованные

        $image = Image::findOrFail($image);
        $quote = Quote::findOrFail($quote);
        //получаем путь к папке с открыткой
        $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        //создаем файл открытки
        $conv = new \Anam\PhantomMagick\Converter();
        $conv
            ->setImageOptions([
                'width' => $image->width,
                'height' => $image->height,
                'quality' => 100
            ])
            ->source('http://78.47.12.238/card/' . $image->id . '?quote=' . $quote->id)
            ->toJpg()
            ->saveLocal($storagePath . 'card.jpg');
        $image->used = 1;
        $image->save();
        $quote->used = 1;
        $quote->save();
    }

    public static function getTagsBySources($image, $quote)
    {
        $image_tags = Image::find($image)->getTagsBySource();
        $quotes_tags = Quote::find($quote)->getTagsBySource();
        return array_merge($image_tags, $quotes_tags);
    }

    //
    public static function addToSchedule($image, $quote, $publish_at)
    {
        //получаем URL для загрузки открытки в ВК
        $vk = new VK(
            Config::get('vk.app_id'),
            Config::get('vk.app_secret'),
            Config::get('vk.access_token')
        );
        $upload_url = $vk->api(
            'photos.getWallUploadServer',
            ['group_id' => Config::get('vk.group_id')]
        )['response']['upload_url'];
//        dd($upload_url);
        sleep(1);
        //создаем файл открытки
        self::createCard($image, $quote);
        //получаем локальный путь к открытке
        $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        //заливаем открытку в ВК
        $post_params = [
            "file1" => curl_file_create($storagePath . 'card.jpg')
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $upload_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        $result = curl_exec($ch);
        curl_close($ch); //
        $result = json_decode($result, true);
        sleep(1);
        //сохраняем открытку в ВК
        $photo_saved = $vk->api(
            "photos.saveWallPhoto",
            [
                "group_id" => Config::get('vk.group_id'),
                "photo" => $result['photo'],
                "server" => $result['server'],
                "hash" => $result['hash'],
            ]
        );
        sleep(1);
        //если открытка сохранена в ВК, добавляем открытку на сайт
        if ($photo_saved['response'][0]['id']) {
            $card = new Card;
            $card->vk_post_id = $photo_saved['response'][0]['id'];
            $card->published = 0;
            $card->publish_at = date("Y-m-d H:i:s", strtotime($publish_at));
            $card->src = $photo_saved['response'][0]['src'];
            $card->src_big = $photo_saved['response'][0]['src_big'];
            $card->quote = $quote;
            $card->save();
            $tags = self::getTagsBySources($image, $quote);
            foreach ($tags as $tag_id => $tag) {
                DB::table('tag_relation')->insert(
                    [
                        'entity_code' => 'card',
                        'entity_id'   => $card->id,
                        'tag_id'      => $tag_id
                    ]
                );
            }
        }
    }

    public static function sendToVkRandom() {
        $card = Card::where('approved', '=', '1')->where('published', '!=', '1')->orderByRaw("RAND()")->first();
        self::sendToVk($card);
    }

    public static function killHistory() {
        $vk = new VK(
            Config::get('vk.app_id'),
            Config::get('vk.app_secret'),
            Config::get('vk.access_token')
        );
        $wall = $vk->api(
            'wall.get',
            [
                'owner_id' => 1549016,
                'offset' => 200,
                'count' => 1000,
                'filter' => 'all',
            ]
        );
        sleep(1);
        if ($wall['response']) {
            foreach ($wall['response'] as $key => $item) {
                if (is_array($item)) {
                    $vk->api(
                        'wall.delete',
                        [
                            'owner_id' => 1549016,
                            'post_id' => $item['id']
                        ]
                    );
                    if ($key % 3 == 0) {
                        sleep(1);
                    }
                }
            }
        }
    }

    public static function sendToVk(Card $card) {
        $vk = new VK(
            Config::get('vk.app_id'),
            Config::get('vk.app_secret'),
            Config::get('vk.access_token')
        );
        $post = [
            'owner_id'      => '-' . Config::get('vk.group_id'),
            'from_group'    => 1,
            'attachments'   => $card->vk_post_id,
            'message'       => "#гуруцитат"
        ];
        if (count($card->getTags())) {
            foreach ($card->getTags() as $tag) {
                $post['message'] .= " #" . str_replace(" ", "", $tag);
            }
        }
        $response = $vk->api('wall.post', $post);
        if (isset($response['response'])) {
            if (isset($response['response']['post_id'])) {
                $card->published = 1;
                $card->save();
            }
        }
    }

    public static function sendToFacebookRandom() {
//        $card = Card::where('published', '!=', '1')->orderByRaw("RAND()")->first();
        return self::sendToFacebook();
    }



    public static function sendToFacebook() {
        $session = FacebookSession::setDefaultApplication(
            Config::get('facebook.app_id'),
            Config::get('facebook.app_secret')
        );
        dd($session);
        $helper = new FacebookRedirectLoginHelper('http://citata.guru/facebook');
        $loginUrl = $helper->getLoginUrl();
        return $loginUrl;
//        $post = [
//            'owner_id'      => '-' . Config::get('vk.group_id'),
//            'from_group'    => 1,
//            'attachments'   => $card->vk_post_id,
//            'message'       => "#гуруцитат"
//        ];
//        if (count($card->getTags())) {
//            foreach ($card->getTags() as $tag) {
//                $post['message'] .= " #" . str_replace(" ", "", $tag);
//            }
//        }
//        $response = $vk->api('wall.post', $post);
//        if (isset($response['response'])) {
//            if (isset($response['response']['post_id'])) {
//                $card->published = 1;
//                $card->save();
//            }
//        }
    }
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use TwitterAPIExchange;
use Config;
use App\Quote;
use DB;

class Twitter extends Model
{

    protected $fillable = [
        'source_url',
        'latest',
    ];

    public function getTags() {
        $selected_tags = [];
        $tumblr_tags = DB::table('tags')
            ->join('tag_relation', 'tags.id', '=', 'tag_relation.tag_id')
            ->where('tag_relation.entity_code', '=', 'twitter')
            ->where('tag_relation.entity_id', '=', $this->id)
            ->select('tags.id','tags.title')
            ->get();
        foreach ($tumblr_tags as $tag) {
            $selected_tags[$tag->id] = $tag->title;
        }
        return $selected_tags;
    }

    public function Korovan()
    {
        $twitter = new TwitterAPIExchange(Config::get('twitter.twitter_settings'));
        $remove = [
            "©",
            "#",
            "«",
            "»",
            '"',
            "(Соня Шаталова)"
        ];
        $twitter_sources = $this->get();
        foreach ($twitter_sources as $twitter_source) {
            $screen_name = str_replace("https://twitter.com/", "", $twitter_source->source_url);
            $get_field = '?screen_name=' . $screen_name . '&count=25';
            if ($twitter_source->latest) {
                $get_field .= '&max_id=' . $twitter_source->latest;
            }
            $response = $twitter->setGetfield($get_field)
                ->buildOauth('https://api.twitter.com/1.1/statuses/user_timeline.json', 'GET')
                ->performRequest();
            $response = json_decode($response, true);
            foreach ($response as $tweet) {
                $rate = $tweet['retweet_count'] + $tweet['favorite_count'];
                if ($rate > 1) {
                    //костыли для удаления всякого говна из текста
                    $text = preg_replace("@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@", '', $tweet['text']);
                    $text = str_replace($remove, "", $text);
                    $text = str_replace("...", ".", $text);
                    if (strstr($text, "@")) {
                        $text = explode("@", $text)[0];
                    }
                    if (strstr($text, "/")) {
                        $text = explode("/", $text)[0];
                    }
                    $quote = Quote::where('twitter_id', '=', $tweet['id'])->orWhere('title', 'like', $text)->first();
                    if(!$quote) {
                        $quote = New Quote;
                        $quote->title = $text;
                        $quote->source = $screen_name;
                        $quote->twitter_id = $tweet['id'];
                        $quote->save();
                        $twitter_source->latest = $tweet['id'];
                        $twitter_source->save();
                    }
                }
            }
            sleep(5);
        }
    }
}

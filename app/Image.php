<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tumblr;

class Image extends Model {

    public function byRatio() {
        return $this->orderByRaw("ROUND(width/height) DESC");
    }

    public function getTagsBySource() {
        return Tumblr::where('source_url', 'like', '%' . $this->source . '%')->first()->getTags();
    }

    public function getNext($approved = '', $used = '') {
        $item = Image::where('id', '>', $this->id);
        if ($approved != 'on') {
            $item = $item->where('approved', '=', null);
        }
        if ($used != 'on') {
            $item = $item->where('used', '!=', '1');
        }
        $item = $item->orderByRaw("ROUND(width/height) DESC")->get();
        if (!$item) {
            $item = Image::get();
        }
        return $item->first()->id;
    }

    public function getPrev($approved = '', $used = '') {
        $item = Image::where('id', '<', $this->id);
        if ($approved != 'on') {
            $item = $item->where('approved', '=', null);
        }
        if ($used != 'on') {
            $item = $item->where('used', '!=', 1);
        }
        $item = $item->orderByRaw("ROUND(width/height) DESC");
        if (!$item) {
            $item = Image::get();
        }
        return $item->first()->id;
    }

}

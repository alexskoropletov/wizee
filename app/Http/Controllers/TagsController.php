<?php namespace App\Http\Controllers;

use App\Tags;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TagsRequest;

use Illuminate\Http\Request;

class TagsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $tags = Tags::paginate(50);
        $page = (int)$request->get('page') ? (int)$request->get('page') : 0;
        return view('tags.index', compact("tags", "page"));
    }

    public function create()
    {
        return view('tags.create');
    }

    public function store(TagsRequest $request)
    {
        Tags::create($request->all());
        return redirect('tag');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tag = Tags::findOrFail($id);
        return view('tags.edit', compact('tag'));
    }

    public function update($id, TagsRequest $request)
    {
        $tag = Tags::findOrFail($id);
        $tag->update($request->all());
        return redirect('tags');
    }

    public function destroy($id)
    {
        //
    }

}

<?php namespace App\Http\Controllers;

use App\Quote;
use App\Twitter;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\QuoteRequest;
use Illuminate\Http\Request;

/**
 * Class QuoteController
 * @package App\Http\Controllers
 */
class QuoteController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $selected_twitters = $request->get('twitters') ? $request->get('twitters') : [];
        $selected_twitters_source = [];
        $approved = $request->get('approved');
        $used = $request->get('used');
        $order = $request->get('order');
        $twitters = [];
        foreach( Twitter::get() as $twitter) {
            $twitters[$twitter->id] = str_replace("https://twitter.com/", "", $twitter->source_url);
            if (in_array($twitter->id, $selected_twitters)) {
                $selected_twitters_source[] = $twitters[$twitter->id];
            }
        }
        $uri = $request->getRequestUri();
        $quotes = new Quote;
        $quotes = $quotes->where('disabled', '!=', '1');
        if (count($selected_twitters)) {
            $quotes = $quotes->whereIn('source', $selected_twitters_source);
        }
        if (!$approved) {
            $quotes = $quotes->where('approved', '!=', '1');
        }
        if (!$used) {
            $quotes = $quotes->where('used', '!=', '1');
        }
        switch ($order) {
            case "ratio":
                $quotes = $quotes->orderByRaw("LENGTH(title)");
                break;
            case "date":
                $quotes = $quotes->orderBy("created_at", "asc");
                break;
            default:
                $quotes = $quotes->orderBy("id", "asc");
        }
        $quotes = $quotes->paginate(10);
        if ($quotes->total() && !$quotes->count()) {
            return redirect('quote?page=' . $quotes->lastPage() . "&approved=" . $approved . "&used=" . $used . "&order=" . $order);
        }
        $page = (int)$request->get('page') ? (int)$request->get('page') : 0;
        return view('quote.index', compact("twitters", "selected_twitters", "uri", "quotes", "page", "approved", "used", "order"));
    }

    public function show($id)
    {
        $quote = Quote::findOrFail($id);
        return view('quote.show', compact("quote"));
    }

    public function create()
    {
        return view('quote.create');
    }

    public function edit($id)
    {
        $quote = Quote::findOrFail($id);
        $selected_twitters = isset($_GET['twitters']) ? $_GET['twitters'] : [];
        return view('quote.edit', compact('quote', 'selected_twitters'));
    }

    public function store(QuoteRequest $request)
    {
        Quote::create($request->all());
        return redirect('quote');
    }

    public function update($id, QuoteRequest $request)
    {
        $quote = Quote::findOrFail($id);
        $quote->update($request->all());
        $link = 'quote?page=' . $request->get('page')
            . '&approved=' . $request->get('approved_pager')
            . '&used=' . $request->get('used_pager')
            . '&order=' . $request->get('order_pager');
        if (is_array($request->get('twitters'))) {
            foreach ($request->get('twitters') as $twitter) {
                $link .= "&twitters[]=" . $twitter;
            }
        }
        return redirect(
            $link . '#quote' . $id
        );
    }

    public function destroy($id, Request $request)
    {
        $quote = Quote::findOrFail($id);
        $quote->delete();
        $link = 'quote?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order');
        if (is_array($request->get('twitters'))) {
            foreach ($request->get('twitters') as $twitter) {
                $link .= "&twitters[]=" . $twitter;
            }
        }
        return redirect(
            $link
        );
    }

    public function approve($id, Request $request)
    {
        $quote = Quote::findOrFail($id);
        $quote->approved = 1;
        $quote->save();
        $link = 'quote?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order');
        if (is_array($request->get('twitters'))) {
            foreach ($request->get('twitters') as $twitter) {
                $link .= "&twitters[]=" . $twitter;
            }
        }
        return redirect(
            $link . '#quote' . $id
        );
    }

    public function unused($id, Request $request)
    {
        $quote = Quote::findOrFail($id);
        $quote->used = null;
        $quote->save();
        $link = 'quote?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order');
        if (is_array($request->get('twitters'))) {
            foreach ($request->get('twitters') as $twitter) {
                $link .= "&twitters[]=" . $twitter;
            }
        }
        return redirect(
            $link . '#quote' . $id
        );
    }

    public function disable($id, Request $request)
    {
        $quote = Quote::findOrFail($id);
        $quote->disabled = 1;
        $quote->save();
        $link = 'quote?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order');
        if (is_array($request->get('twitters'))) {
            foreach ($request->get('twitters') as $twitter) {
                $link .= "&twitters[]=" . $twitter;
            }
        }
        return redirect(
            $link . '#quote' . $id
        );
    }

}

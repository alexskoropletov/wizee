<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CardRequest;
use App\Quote;
use App\Image;
use App\Tags;
use App\Card;
use App\Tumblr;
use Illuminate\Support\Facades\Config;
use Storage;
use DB;

class CardController extends Controller
{
    public $filters = [
        '',
        'blur',
        'grayscale',
        'saturate',
        'ig-xpro2',
        'ig-walden',
        'ig-valencia',
        'ig-toaster',
        'ig-rise',
        'ig-nashville',
        'ig-mayfair',
        'ig-lofi',
        'ig-hudson',
        'ig-amaro',
        'ig-1977'
    ];

    public $fonts = [
        "'PT Sans Narrow', sans-serif",
        "'Ubuntu Condensed', sans-serif",
        "'Lobster', cursive",
    ];

    public $frame = [
        "none",
        "block"
    ];

    public $positions = [
        1.62,
        2,
        3,
    ];

    public $shadows = [
        "text-black-shadow-full",
        "text-white-shadow-full",
        "text-black-shadow",
        "text-white-shadow",
    ];

    public $font_sizes = [
        "2em",
        "3em",
        "4em",
        "5em",
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('card.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function oftheday()
    {
        Card::setOfTheDay();
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $quotes = Quote::where("approved", "=", "1")->where("used", "=", "0")->orderByRaw("LENGTH(title)")->get();
        $images = Image::where("approved", "=", "1")->where("used", "=", "0")->orderByRaw("ROUND(height/width * 100) DESC")->paginate(500);
        $latest_card = Card::where('published', '=', '0')->orderBy('publish_at', 'desc')->first();
        if (!$latest_card) {
            $time = time() + Config::get("vk.posting_period") + rand(0, Config::get("vk.posting_spread"));
        } else {
            $time = strtotime($latest_card->publish_at);
            if ($time < time()) {
                $time = time() + Config::get("vk.posting_period") + rand(0, Config::get("vk.posting_spread"));
            }
        }
        $time = date("d.m.Y H:i:s", $time);

        $fonts = $this->fonts;
        $font_sizes = $this->font_sizes;
        $positions = $this->positions;
        $shadows = $this->shadows;
        $filters = $this->filters;
        return view(
            'card.create',
            compact("images", "quotes", "time", "filters", "fonts", "positions", "shadows", "font_sizes")
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function queue(Request $request)
    {
        $cards = Card::orderBy('id', 'desc')->paginate(15);
        $page = (int)$request->get('page') or 1;
        return view('card.queue', compact("cards", "page"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CardRequest $request
     * @return Response
     */
    public function store(CardRequest $request)
    {
        Card::addToSchedule(
            $request->get('image'),
            $request->get('quote'),
            $request->get('publish_at')
        );
        return view('card.ready');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function show($id, Request $request)
    {
        $quote = Quote::findOrFail($request->get('quote'));
        $image = Image::findOrFail($id);
        $text_width = $image->width - 50;
        $frame_margin = 15;
        if ($image->width > 1000) {
            $frame_margin = 40;
            $text_width -= 100;
        }

        $position = $request->get('position') ? $request->get('position') : 1.62;
        if ($image->height > $image->width) {
            /*vertical*/
            $text_height = round($image->height / $position);
        } else {
            /*horizontal*/
            $text_height = round($image->height / $position);
        }

        $font_size = "2em";
        if ($image->height > 500) {
            $font_size = "3em";
        }
        if ($image->height > 1000) {
            $font_size = "4em";
        }
        if ($image->height > 1400) {
            $font_size = "5em";
        }
        $font_size = $request->get('font_size') ? $request->get('font_size') : $font_size;

        $filter = $request->get('filters') ? $request->get('filters') : $this->filters[rand(0, count($this->filters) - 1)];
        $frame = $this->frame[rand(0, 1)];
        $font = $request->get('font') ? $request->get('font') : $this->fonts[rand(0, count($this->fonts) - 1)];
        $shadow = $request->get('shadow') ? $request->get('shadow') : 'text-black-shadow-full';

        $text = str_replace(
            [".", ":", "?", "!"],
            [".<br>", ":<br>", "?<br>", "!<br>"],
            $quote->title
        );
        return view(
            'card.card',
            compact('image', 'text_width', 'text_height', 'text', 'filter', 'font', 'font_size', 'shadow', 'frame', 'frame_margin')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $card = Card::findOrFail($id);
        $tags = [];
        foreach(Tags::get() as $tag) {
            $tags[$tag->id] = $tag->title;
        }
        return view('card.edit', compact('card', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {

        DB::table('tag_relation')
            ->where('entity_code', '=', 'card')
            ->where('entity_id', '=', $id)
            ->delete();
        foreach ($request->get('tags') as $tag_id) {
            DB::table('tag_relation')->insert(
                [
                    'entity_code' => 'card',
                    'entity_id'   => $id,
                    'tag_id'      => $tag_id
                ]
            );
        }
        return redirect('card/queue?page=' . $request->get('page'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $card = Card::findOrFail($id);
        $card->delete();
        return redirect('card/queue');
    }

    public function publish($id, Request $request)
    {
        $card = Card::findOrFail($id);
        Card::sendToVk($card);
        return redirect('card/queue?page=' . $request->get('page') . "#card" . $card->id);
    }

    public function approve($id, Request $request)
    {
        $card = Card::findOrFail($id);
        $card->approved = !$card->approved;
        $card->save();
        return redirect('card/queue?page=' . $request->get('page'));
    }

    public function tumblr($id, Request $request)
    {
        $card = Card::findOrFail($id);
        Tumblr::postPhoto($card->src_big);
//        return redirect('card/queue?page=' . $request->get('page') . "#card" . $card->id);
    }

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Twitter;
use Illuminate\Http\Request;
use App\Card;
use App\Quote;
use App\Image;
use App\Tags;
use DB;
use Config;
use Illuminate\Support\Debug\HtmlDumper;
use VK\VK;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Log;

class PageController extends Controller {

    public function index(Request $request)
    {
        $url = $request->url();
        $card = Card::where('approved', '=', '1')->orderByRaw("RAND()")->first();
        if ($card->quote) {
            $quote = Quote::find($card->quote)->title;
        } else {
            $quote = '';
        }
        $title = '';
        return view('pages.home', compact('card', 'quote', 'title', 'url'));
    }

    public function about()
    {
        return view('pages.about');
    }

    public function newPage()
    {
        $oftheday = Card::where('approved', '=', '1')->where('oftheday', '=', '1')->first();
        $quote = '';
        if ($oftheday->quote) {
            $quote = Quote::find($oftheday->quote)->title;
        }
        $latest = Card::where('approved', '=', '1')->orderBy('id', 'desc')->paginate(2);
        $nopicture = array(
            'humor' => $this->getUnusedQuotesByTag(11),
            'motivation' => $this->getUnusedQuotesByTag(9),
            'love' => $this->getUnusedQuotesByTag(2),
            'wisdom' => $this->getUnusedQuotesByTag(8),
        );
        return view('pages.newpage', compact("oftheday", "nopicture", "quote", "latest"));
    }



    public function newlook()
    {
        $oftheday = Card::where('approved', '=', '1')->where('oftheday', '=', '1')->first();
        $quote = '';
        if ($oftheday->quote) {
            $quote = Quote::find($oftheday->quote)->title;
        }
        $latest = Card::where('approved', '=', '1')->orderBy('id', 'desc')->paginate(2);
        $nopicture = array(
            'humor' => $this->getUnusedQuotesByTag(11),
            'motivation' => $this->getUnusedQuotesByTag(9),
            'love' => $this->getUnusedQuotesByTag(2),
            'wisdom' => $this->getUnusedQuotesByTag(8),
        );
        return view('pages.newlook', compact("oftheday", "nopicture", "quote", "latest"));
    }

    public function facebook()
    {
        $vk = new VK(
            Config::get('vk.app_id'),
            Config::get('vk.app_secret'),
            Config::get('vk.access_token')
        );
        $filter = [
            'count' => 100,
        ];
        $wall = $vk->api('newsfeed.getComments', $filter);
        sleep(1);
        if ($wall['response']) {
            $items = [];
            foreach ($wall['response']['items'] as $key => $item) {
//                echo "<br>" . $key . "<br>";
                if (is_array($item)) {
                    $comment_filter = [
                        'owner_id' => $item['source_id'],
                        'post_id' => $item['post_id'],
                        'count' => 100,
                        'preview_length' => 0,
                    ];
                    $item['comments_arr'] = $vk->api('wall.getComments', $comment_filter);
                    $item['filter'] = $comment_filter;
                    $items[] = $item;
                }
                if ($key % 2 == 0) {
                    sleep(1);
                }
            }
            return view('pages.facebook', compact('items'));
        }
        return "";
    }

    public function statistic()
    {
        $quotes = Quote::get();
        $images = Image::get();
        $cards = Card::get();
        return view('pages.statistic', compact('quotes', 'images', 'cards'));
    }

    public function tagcloud()
    {
        $tags = Tags::orderBy('title', 'asc')->get();
        return view('pages.tagcloud', compact('tags'));
    }

    public function allcards(Request $request)
    {
        $cards = Card::where('approved', '=', '1')->orderBy('id', 'desc')->paginate(6);
        $page = (int)$request->get('page') ? (int)$request->get('page') : 0;
        return view('pages.allcards', compact("cards", "page"));
    }

    public function getUnusedQuotesByTag($tag_id = 1)
    {
        $twitters = DB::table('tag_relation')
            ->where('entity_code', '=', 'twitter')
            ->where('tag_id', '=', $tag_id)
            ->orderBy('entity_id', 'desc')
            ->get();
        $select_me = [];
        if(count($twitters)) {
            foreach ($twitters as $twitter) {
                $select_me[] = $twitter->entity_id;
            }
        }
        $twitters = Twitter::whereIn("id", $select_me)->get();
        $select_me = [];
        if(count($twitters)) {
            foreach ($twitters as $twitter) {
                $select_me[] = str_replace("https://twitter.com/", "", $twitter->source_url);
            }
        }
        return Quote::
            where('approved', '=', '1')
            ->whereIn("source", $select_me)
            ->where('used', '=', '0')
            ->orderBy('id', 'desc')
            ->paginate(10);
    }

    public function view($id, Request $request)
    {
        $url = $request->url();
        $card = Card::find($id);
        if (!$card || !$card->approved) {
            return redirect("/");
        }
        if ($card->quote) {
            $quote = Quote::find($card->quote)->title;
        } else {
            $quote = '';
        }
        $title = strip_tags($quote);
        return view('pages.home', compact('card', 'quote', 'title', 'url'));
    }

    public function block(Request $request)
    {
        $cards = Card::where('approved', '=', '1')->orderBy("id", "desc");
        if ($request->get('tag')) {
            $cards_ids = DB::table('tag_relation')
                ->where('entity_code', '=', 'card')
                ->where('tag_id', '=', $request->get('tag'))
                ->orderBy('entity_id', 'desc')
                ->get();
            if(count($cards_ids)) {
                $select_me = [];
                foreach ($cards_ids as $card_id) {
                    $select_me[] = $card_id->entity_id;
                }
                $cards = $cards->whereIn("id", $select_me);
            } else {
                return '';
            }
        }
        $cards = $cards->paginate(6);
        if ($cards->count()) {
            return view('pages.block', compact('cards', 'highlight'));
        } else {
            return '';
        }
    }

    public function tag($id, Request $request)
    {
        $tag = Tags::findOrFail($id);
        $cards_ids = DB::table('tag_relation')
            ->where('entity_code', '=', 'card')
            ->where('tag_id', '=', $tag->id)
            ->orderBy('entity_id', 'desc')
            ->get();
        if(count($cards_ids)) {
            $select_me = [];
            foreach ($cards_ids as $card_id) {
                $select_me[] = $card_id->entity_id;
            }
        }
        $cards = Card::where('approved', '=', '1')->whereIn("id", $select_me)->orderBy('id', 'desc')->paginate(6);
        $page = (int)$request->get('page') ? (int)$request->get('page') : 0;
        return view('pages.allcards', compact("cards", "page", "tag"));
    }

    public function endless(Request $request)
    {
        return view('pages.endless', ['tag' => '']);
    }
}

<?php namespace App\Http\Controllers;

use App\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ImageController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
     * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
        $images = new Image;
        $images = $images->where('disabled', '!=', '1');
        $approved = $request->get('approved');
        $used = $request->get('used');
        $order = $request->get('order');
        if ($approved != 'on') {
            $images = $images->where('approved', '=', null);
        }
        if ($used != 'on') {
            $images = $images->where('used', '!=', '1');
        }
        if ($used != 'on') {
            $images = $images->where('used', '!=', '1');
        }
        switch ($order) {
            case "ratio":
                $images = $images->orderByRaw("ROUND(width/height) DESC");
                break;
            case "date":
                $images = $images->orderBy("created_at", "asc");
                break;
            default:
                $images = $images->orderBy("id", "asc");
        }
        $images = $images->paginate(10);
        if ($images->total() && !$images->count()) {
            return redirect('image?page=' . $images->lastPage() . "&approved=" . $approved . "&used=" . $used . "&order=" . $order);
        }
        $page = (int)$request->get('page') ? (int)$request->get('page') : 0;
        return view('images.index', compact("images", "page", "approved", "used", "order"));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

    public function destroy($id, Request $request)
    {
        $image = Image::findOrFail($id);
        $image->delete();
        return redirect(
            'image?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order')
        );
    }

    public function approve($id, Request $request)
    {
        $image = Image::findOrFail($id);
        $image->approved = 1;
        $image->save();
        return redirect(
            'image?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order')
            . '#image' . $id
        );
    }

    public function unused($id, Request $request)
    {
        $image = Image::findOrFail($id);
        $image->used = null;
        $image->save();
        return redirect(
            'image?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order')
            . '#image' . $id
        );
    }

    public function disable($id, Request $request)
    {
        $image = Image::findOrFail($id);
        $image->disabled = 1;
        $image->save();
        return redirect(
            'image?page=' . $request->get('page')
            . '&approved=' . $request->get('approved')
            . '&used=' . $request->get('used')
            . '&order=' . $request->get('order')
            . '#image' . $id
        );
    }

}

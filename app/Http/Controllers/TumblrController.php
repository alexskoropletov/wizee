<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Tumblr;
use App\Card;
use App\Tags;
use App\Http\Requests\TumblrRequest;
use DB;

class TumblrController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
//        return redirect("https://oauth.vk.com/access_token?client_id=4866360&client_secret=9dgMo89URXGLdQJH1CLV&code=4f380b39d3261320a0");
//        $fblink = Card::sendToFacebookRandom();
//        dd($fblink);
//        Card::createRandomCard();
        $tumblrs = Tumblr::latest()->get();
        return view('tumblr.index', compact("tumblrs"));
    }

    public function create()
    {
        return view('tumblr.create');
    }

    public function store(TumblrRequest $request)
    {
        Tumblr::create($request->all());
        return redirect('tumblr');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tags = [];
        foreach(Tags::get() as $tag) {
            $tags[$tag->id] = $tag->title;
        }
        $tumblr = Tumblr::findOrFail($id);
        return view('tumblr.edit', compact('tumblr', 'tags'));
    }

    public function update($id, TumblrRequest $request)
    {
        $tumblr = Tumblr::findOrFail($id);
        $tumblr->update($request->all());
        DB::table('tag_relation')
            ->where('entity_code', '=', 'tumblr')
            ->where('entity_id', '=', $id)
            ->delete();
        foreach ($request->get('tags') as $tag_id) {
            DB::table('tag_relation')->insert(
                [
                    'entity_code' => 'tumblr',
                    'entity_id'   => $id,
                    'tag_id'      => $tag_id
                ]
            );
        }
        return redirect('tumblr');
    }

    public function destroy($id)
    {
        $item = Tumblr::findOrFail($id);
        $item->delete();
        return redirect('tumblr');
    }

}

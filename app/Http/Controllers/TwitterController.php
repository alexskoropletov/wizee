<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Twitter;
use App\Tags;
use App\Http\Requests\TwitterRequest;
use DB;

/**
 * Class TwitterController
 * @package App\Http\Controllers
 */
class TwitterController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
        $twitters = Twitter::latest()->get();
        return view('twitter.index', compact("twitters"));
	}

	public function create()
	{
        $tags = [];
        foreach(Tags::get() as $tag) {
            $tags[$tag->id] = $tag->title;
        }
        $selected_tags = [];
        return view('twitter.create', compact('tags', 'selected_tags'));
	}

	public function store(TwitterRequest $request)
	{
        $twitter = Twitter::create($request->all());
        DB::table('tag_relation')
            ->where('entity_code', '=', 'twitter')
            ->where('entity_id', '=', $twitter->id)
            ->delete();
        foreach ($request->get('tags') as $tag_id) {
            DB::table('tag_relation')->insert(
                [
                    'entity_code' => 'twitter',
                    'entity_id'   => $twitter->id,
                    'tag_id'      => $tag_id
                ]
            );
        }
        return redirect('twitter');
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
        $tags = [];
        foreach(Tags::get() as $tag) {
            $tags[$tag->id] = $tag->title;
        }
        $twitter = Twitter::findOrFail($id);
        $selected_tags = $twitter->getTags();
        return view('twitter.edit', compact('twitter', 'tags', 'selected_tags'));
	}

    public function update($id, TwitterRequest $request)
    {
        $twitter = Twitter::find($id);
        $twitter->update($request->all());
        DB::table('tag_relation')
            ->where('entity_code', '=', 'twitter')
            ->where('entity_id', '=', $id)
            ->delete();
        foreach ($request->get('tags') as $tag_id) {
            DB::table('tag_relation')->insert(
                [
                    'entity_code' => 'twitter',
                    'entity_id'   => $id,
                    'tag_id'      => $tag_id
                ]
            );
        }
        return redirect('twitter');
    }

	public function destroy($id)
	{
        $item = Twitter::findOrFail($id);
        $item->delete();
        return redirect('twitter');
	}

}

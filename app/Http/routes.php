<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@newlook');
Route::get('/home', 'PageController@newlook');
//Route::get('/', 'PageController@endless');
//Route::get('/home', 'PageController@endless');
Route::get('/endless', 'PageController@endless');
Route::get('/random', 'PageController@index');
Route::get('/statistic', ['middleware' => 'auth', 'uses' => 'PageController@statistic']);
Route::get('/view/{id}', 'PageController@view');
Route::get('/tag/{id}', 'PageController@tag');
Route::get('/tagcloud', 'PageController@tagcloud');
Route::get('/allcards', 'PageController@allcards');
Route::get('/block', 'PageController@block');
Route::get('/about', 'PageController@about');
Route::get('/facebook', 'PageController@facebook');
Route::get('/new', 'PageController@newPage');

//Quotes
Route::resource('quote', 'QuoteController');
Route::get('quote/{id}/destroy', ['middleware' => 'auth', 'uses' => 'QuoteController@destroy']);
Route::get('quote/{id}/approve', ['middleware' => 'auth', 'uses' => 'QuoteController@approve']);
Route::get('quote/{id}/unused', ['middleware' => 'auth', 'uses' => 'QuoteController@unused']);
Route::get('quote/{id}/disable', ['middleware' => 'auth', 'uses' => 'QuoteController@disable']);

//Images
Route::resource('image', 'ImageController');
Route::get('image/{id}/destroy', ['middleware' => 'auth', 'uses' => 'ImageController@destroy']);
Route::get('image/{id}/approve', ['middleware' => 'auth', 'uses' => 'ImageController@approve']);
Route::get('image/{id}/unused', ['middleware' => 'auth', 'uses' => 'ImageController@unused']);
Route::get('image/{id}/disable', ['middleware' => 'auth', 'uses' => 'ImageController@disable']);

//Twitter sources
Route::resource('twitter', 'TwitterController');
Route::get('twitter/{id}/destroy', ['middleware' => 'auth', 'uses' => 'TwitterController@destroy']);

//Tumblr sources
Route::resource('tumblr', 'TumblrController');
Route::get('tumblr/{id}/destroy', ['middleware' => 'auth', 'uses' => 'TumblrController@destroy']);

//Tags sources
Route::get('tags/create', ['middleware' => 'auth', 'uses' => 'TagsController@create']);
Route::resource('tag', 'TagsController');
Route::get('tag/{id}/destroy', ['middleware' => 'auth', 'uses' => 'TagsController@destroy']);

//Cards
Route::get('card/oftheday', ['middleware' => 'auth', 'uses' => 'CardController@oftheday']);
Route::get('card/queue', 'CardController@queue');
Route::resource('card', 'CardController');
Route::get('card/{id}/destroy', ['middleware' => 'auth', 'uses' => 'CardController@destroy']);
Route::get('card/{id}/publish', ['middleware' => 'auth', 'uses' => 'CardController@publish']);
Route::get('card/{id}/approve', ['middleware' => 'auth', 'uses' => 'CardController@approve']);
Route::get('card/{id}/tumblr', ['middleware' => 'auth', 'uses' => 'CardController@tumblr']);

Route::get('pics/{image}', function($image = null)
{
    $path = storage_path().'/app/' . $image;
    if (file_exists($path)) {
        return Response::download($path);
    }
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

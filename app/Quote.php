<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.03.15
 * Time: 14:41
 */

namespace App;

use Carbon\Carbon;
use App\Twitter;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model {

    protected $table = 'quote';

    protected $fillable = [
        'title',
        'source',
        'twitter_id',
        'approved',
        'published_at',
    ];

    protected $dates = ['published_at'];

    public function getTagsBySource() {
        return Twitter::where('source_url', '=', 'https://twitter.com/' . $this->source)->first()->getTags();
    }

    public function card()
    {
        return $this->belongsTo('App\Card');
    }

    public function getNext() {
        $item = Image::Quote('id', '>', $this->id)->first();
        if (!$item) {
            $item = Image::get()->first();
        }
        return $item->id;
    }

    public function getPrev() {
        $item = Image::Quote('id', '<', $this->id)->first();
        if (!$item) {
            $item = Image::get()->last();
        }
        return $item->id;
    }

    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeExists($query, $id, $text)
    {
        $query->where('twitter_id', '=', $id);
        $query->where('title', 'like', $text);
    }

    public function scopeUnpublished($query)
    {
        $query->where('published_at', '>=', Carbon::now());
    }

    public function setApprovedAttribute($approved)
    {
        if (!is_numeric($approved)) {
            $this->attributes['approved'] = $approved == "on" ? 1 : 0;
        } else {
            $this->attributes['approved'] = $approved;
        }
    }

//    public function setPublishedAtAttribute($date)
//    {
//        $this->attributes['published_at'] = strtotime($date);
////        $this->attributes['published_at'] = Carbon::parse("Y-m-d", $date); //полночь
//    }
} 
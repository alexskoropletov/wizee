<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tags extends Model {

    protected $fillable = [
        'title'
    ];

    public function getCount() {
        return DB::table('tag_relation')
            ->where('entity_code', '=', 'card')
            ->where('tag_id', '=', $this->id)
            ->select('id')
            ->count();
    }

}

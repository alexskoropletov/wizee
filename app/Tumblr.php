<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Tumblr\API\Client;
use VK\VK;
use Config;
use DB;

class Tumblr extends Model {

    protected $fillable = [
        'source_url',
        'latest',
    ];

    public function getTags() {
        $selected_tags = [];
        $tumblr_tags = DB::table('tags')
            ->join('tag_relation', 'tags.id', '=', 'tag_relation.tag_id')
            ->where('tag_relation.entity_code', '=', 'tumblr')
            ->where('tag_relation.entity_id', '=', $this->id)
            ->select('tags.id','tags.title')
            ->get();
        foreach ($tumblr_tags as $tag) {
            $selected_tags[$tag->id] = $tag->title;
        }
        return $selected_tags;
    }

    public function Korovan() {
        $options = ['type' => 'photo'];
        $tumblrs = Tumblr::get();
        $tumblr_client = new Client(
            Config::get('tumblr.consumer_key'),
            Config::get('tumblr.consumer_secret')
        );
        foreach ($tumblrs as $tumblr) {
            $options['offset'] = (int)$tumblr->latest;
            $source_url = str_replace(['https:', 'http:', '.tumblr.com', '/'], '', $tumblr->source_url);
            $response = $tumblr_client->getBlogPosts($source_url, $options);
            foreach ($response->posts as $post) {
                if (
//                    $post->photos[0]->original_size->width >= $post->photos[0]->original_size->height
                    !strpos($post->photos[0]->original_size->url, ".gif")
                    && (
                        strpos($post->photos[0]->original_size->url, "_1280")
                        || strpos($post->photos[0]->original_size->url, "1280_")
                    )
                ) {
                    $image = Image::where('image_url', '=', $post->photos[0]->original_size->url)->first();
                    if(!$image) {
                        $image = new Image;
                        $image->image_url = $post->photos[0]->original_size->url;
                        $image->width = $post->photos[0]->original_size->width;
                        $image->height = $post->photos[0]->original_size->height;
                        $image->preview_url = $post->photos[0]->alt_sizes[4]->url;
                        $image->bigger_preview_url = $post->photos[0]->alt_sizes[3]->url;
                        $image->source = $source_url;
                        $image->save();
                        $tumblr->latest = $options['offset'] + 20;
                        $tumblr->save();
                    }
                }
            }
            sleep(5);
        }
    }

    public static function postPhoto($url) {
        echo "<pre>";
        var_dump($url);
        $tumblr_client = new Client(
            Config::get('tumblr.consumer_key'),
            Config::get('tumblr.consumer_secret')
        );
        $tumblr_client->getRequestHandler()->setBaseUrl('https://www.tumblr.com/');
        $req = $tumblr_client->getRequestHandler()->request('POST', 'oauth/request_token', [
            'oauth_callback' => 'http://citata.guru/card/1686/tumblr',
        ]);
//
//// Get the result
        $result = explode("&", $req->body->__toString());
        foreach($result as $val) {
            $val = explode("=", $val);
            $result[$val[0]] = $val[1];
        }
        var_dump($result);
        $tumblr_client->setToken(
            $result['oauth_token'],
            $result['oauth_token_secret']
        );
        $tumblr_client->getRequestHandler()->setBaseUrl('https://api.tumblr.com/');
        $result = $tumblr_client->createPost(
            'citataguru',
//            'https://www.tumblr.com/blog/citataguru',
            array(
                'type' => 'photo',
                'source' => $url
            )
        );
        var_dump($result);
    }

}

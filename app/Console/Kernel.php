<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Twitter;
use App\Tumblr;
use App\Card;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{


        $schedule->call(function() {
            Card::setOfTheDay();
        })->dailyAt("00:01");

//        грабим корованы
        $schedule->call(function() {
            $grabber = new Twitter;
            $grabber->Korovan();
        })->twiceDaily();
        $schedule->call(function() {
            $grabber = new Tumblr;
            $grabber->Korovan();
        })->daily();

//        генерим открытки
        $generate_at = [
            "01:00",
            "01:10",
            "01:20",
            "01:30",
            "01:40",
            "01:50",
            "02:00",
            "02:10",
            "02:20",
            "02:30",
        ];
        foreach ($generate_at as $time) {
            $schedule->call(function() {
                Card::createRandomCard();
            })->dailyAt($time);
        }

        //отправляем в ВК
        $generate_at = [
            "07:45",
            "08:30",
            "09:40",
            "17:45",
            "18:35",
            "19:25",
            "20:10",
            "20:40",
        ];
        foreach ($generate_at as $time) {
            $schedule->call(function() {
                Card::sendToVkRandom();
            })->dailyAt($time);
        }
        //право на забвение
        $schedule->call(function() {
            Card::killHistory();
        })->hourly();
    }

}

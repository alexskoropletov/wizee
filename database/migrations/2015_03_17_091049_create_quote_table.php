<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quote', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->string('source')->nullable();
            $table->string('twitter_id')->nullable();
            $table->boolean('approved')->default(0);
            $table->timestamp('published_at');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('quote');
	}

}

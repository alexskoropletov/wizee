@extends('app')

@section('content')
<div class="row">

</div>
<div class="jumbotron">
    <h1>Ошибка</h1>
    <p>Извините, похоже что такой страницы на сайте нет</p>
    <p>Попробуйте начать с <a class="btn btn-primary btn-lg" href="/" role="button">Главной</a></p>
</div>
@stop
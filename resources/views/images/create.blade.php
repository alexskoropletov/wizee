@extends('app')

@section('content')
    <h1>Добавить цитату</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::open(['url' => 'image']) !!}
        @include ('images.form')
    {!! Form::close() !!}
@stop
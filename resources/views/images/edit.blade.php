@extends('app')

@section('content')
    <h1>Редактирование цитаты</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::model($image, ['method' => 'PATCH', 'action' => ['QuoteController@update', $image->id]]) !!}
        @include ('images.form')
    {!! Form::close() !!}
@stop
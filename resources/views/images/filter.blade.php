{!! Form::open(['method' => 'GET', 'url' => 'image']) !!}
    <div class="form-group">
        {!! Form::label('approved', 'Одобренные') !!}
        {!! Form::checkbox('approved', null, ['class' => 'form-control']) !!}
        &nbsp;&nbsp;&nbsp;
        {!! Form::label('used', 'Опубликованые') !!}
        {!! Form::checkbox('used', null, ['class' => 'form-control']) !!}
        &nbsp;&nbsp;&nbsp;
        {!! Form::label('used', 'Сортировка') !!}
        {!! Form::select('order', ['id' => 'id', 'date' => 'дате', 'ratio' => 'размеру'], ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Показать', ['class' => 'btn btn-primary form-control']) !!}
    </div>
{!! Form::close() !!}
@extends('app')

@section('content')
    <h1>Список картинок</h1>
    <div>
        <table width="100%">
            <tr>
                <td>
                    {!! $images->appends(['approved' => $approved, 'used' => $used, 'order' => $order])->render() !!}
                </td>
                <td align="right">
                    <a href="#" id="show_list_index_filter">Показать фильтр</a>
                </td>
            </tr>
        </table>
        <div id="list_index_filter" style="display: none;">
            @include ('images.filter')
        </div>
    </div>
    <table class="table table-bordered">
        @foreach($images as $image)
            <tr id="image{{ $image->id }}" @if (!$image->approved) class="danger" @endif @if ($image->used) class="success" @endif>
                <td scope="row">
                    <b>{{ $image->id }}</b>
                </td>
                <td>
                    <div class="btn-toolbar" role="toolbar" aria-label="Tools">
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/image/{{ $image->id }}/edit?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/image/{{ $image->id }}/approve?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}" alt="Одобрить">
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/image/{{ $image->id }}/unused?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}" alt="Не использовано">
                                <span class="glyphicon glyphicon-ok"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/image/{{ $image->id }}/disable?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}" alt="Не использовано">
                                <span class="glyphicon glyphicon-minus"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/image/{{ $image->id }}/destroy?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </div>
                    </div>
                </td>
                <td>
                    <a class="lightbox" href="{{ $image->image_url }}">
                        <img src="{{ $image->bigger_preview_url or $image->preview_url }}" class="img-thumbnail" />
                    </a>
                </td>
                <td>
                    {{ $image->width }} X {{ $image->height }}
                </td>
                <td>{{ implode(", ", $image->getTagsBySource()) }}</td>
                <td>{{ $image->source }}</td>
            </tr>
        @endforeach
    </table>
    {!! $images->appends(['approved' => $approved, 'used' => $used, 'order' => $order])->render() !!}
@stop
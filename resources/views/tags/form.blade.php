<div class="form-group">
    {!! Form::label('title', 'Тег') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) !!}
</div>
@extends('app')

@section('content')
    <h1>Редактирование тега</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::model($tag, ['method' => 'PATCH', 'action' => ['TagsController@update', $tag->id]]) !!}
        <input type="hidden" name="page" value="<?=$_GET['page']?>">
        @include ('tags.form')
    {!! Form::close() !!}
@stop
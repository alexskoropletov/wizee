@extends('app')

@section('content')
    <h1>Добавить тэг</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::open(['url' => 'tag']) !!}
        @include ('tags.form')
    {!! Form::close() !!}
@stop
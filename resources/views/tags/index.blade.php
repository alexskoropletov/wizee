@extends('app')

@section('content')
    <h1>Список тегов</h1>
    <hr/>
    <a href="tags/create">Добавить</a>
    <hr/>
    {!! $tags->render() !!}
    <table class="table table-bordered">
        @foreach($tags as $tag)
            <tr id="tag{{ $tag->id }}">
                <td scope="row">
                    <b>{{ $tag->id }}</b>
                </td>
                <td>
                    <div class="btn-toolbar" role="toolbar" aria-label="Tools">
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/tag/{{ $tag->id }}/edit?page={{ $page }}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/tag/{{ $tag->id }}/destroy?page={{ $page }}">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </div>
                    </div>
                </td>
                <td><a href="/tag/{{ $tag->id }}">{{ $tag->title }}</a></td>
            </tr>
        @endforeach
    </table>
    {!! $tags->render() !!}
@stop
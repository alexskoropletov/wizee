@extends('app')

@section('content')
{!! $cards->render() !!}
<table class="table table-bordered">
    <tr>
        <td scope="row">
            Номер
        </td>
        <td>
            Действия
        </td>
        <td>
            Превью
        </td>
        <td>
            Теги
        </td>
        <td>
            Дата сохдания
        </td>
    </tr>
    @foreach($cards as $card)
    <tr id="card{{ $card->id }}" @if ($card->published) class="success" @endif>
        <td>
            <b>{{ $card->id }}</b>
        </td>
        <td scope="row">
            <div class="btn-toolbar" role="toolbar" aria-label="Tools">
                <div class="btn-group" role="group" aria-label="Tools">
                    <a href="/card/{{ $card->id }}/edit?page={{ $page }}">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </div>
                <div class="btn-group" role="group" aria-label="Tools">
                    <a href="/card/{{ $card->id }}/publish?page={{ $page }}" alt="Опубликовать">
                        <span class="glyphicon glyphicon-check"></span>
                    </a>
                </div>
                <div class="btn-group" role="group" aria-label="Tools">
                    <a href="/card/{{ $card->id }}/destroy">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
                <div class="btn-group" role="group" aria-label="Tools">
                    &nbsp;
                </div>
                <div class="btn-group" role="group" aria-label="Tools">
                    <a href="/card/{{ $card->id }}/approve">
                        <span class="glyphicon @if ($card->approved) glyphicon-ok-circle @else glyphicon-ban-circle @endif"></span>
                    </a>
                </div>
                <div class="btn-group" role="group" aria-label="Tools">
                    &nbsp;
                </div>
                <div class="btn-group" role="group" aria-label="Tools">
                    <a href="/card/{{ $card->id }}/tumblr">
                        <span class="glyphicon glyphicon-star"></span>
                    </a>
                </div>
            </div>
        </td>
        <td>
            <a class="lightbox" href="{{ $card->src_big }}">
                <img src="{{ $card->src }}" class="img-thumbnail" />
            </a>
        </td>
        <td>
            {{ implode(", ", $card->getTags()) }}
        </td>
        <td>
            {{ $card->created_at }}
        </td>
    </tr>
    @endforeach
</table>
{!! $cards->render() !!}
@stop
@extends('app')

@section('content')
    <h1>Создание открыток</h1>
    {!! Form::open(['url' => 'card']) !!}
    <table class="table table-bordered">
        <tr>
            <td>
                <div class="form-group" style="height: 340px; overflow: auto;">
                    <ul class="list-group">
                        @foreach($quotes as $quote)
                        <li class="list-group-item quote" data-id="{{ $quote->id }}" id="quote{{ $quote->id }}cover">
                            {{ $quote->title }}
                            <div style="width: 1px; height: 1px; overflow: hidden;">
                                <input class='hidden_radio_quote' type="radio" name="quote" id="quote{{ $quote->id }}" value="{{ $quote->id }}" />
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </td>
            <td>
                <div class="form-group" style="height: 320px; width: 600px; overflow: auto;">
                    @foreach($images as $image)
                    <div style='padding: 5px 5px 5px 5px; float: left; width: 110px; height: 160px; overflow: hidden;' class="list-group-item image" data-id="{{ $image->id }}" id="image{{ $image->id }}cover">
                        <img src="{{ $image->preview_url }}" />
                        <div style="width: 1px; height: 1px; overflow: hidden;">
                            <input class='hidden_radio_image' type="radio" name="image" id="image{{ $image->id }}" value="{{ $image->id }}" />
                        </div>
                    </div>
                    @endforeach
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <select name="font" class="form-control">
                        @foreach ($fonts as $item)
                            <option value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select name="font_size" class="form-control">
                        @foreach ($font_sizes as $item)
                            <option value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select name="position" class="form-control">
                        @foreach ($positions as $item)
                            <option value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select name="shadow" class="form-control">
                        @foreach ($shadows as $item)
                            <option value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select name="filters" class="form-control">
                        @foreach ($filters as $item)
                            <option value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::input('date', 'publish_at', $time, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <button type="button" id='show_preview' class="btn btn-default">Предпросмотр</button>
                </div>
                <div class="form-group">
                    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </td>
            <td>
                <div id="preview" style="width: 640px; height: 640px; border: 1px solid black;"></div>
            </td>
        </tr>
    </table>
    {!! Form::close() !!}
@stop
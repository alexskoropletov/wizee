<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Scada&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
</head>
<body>
<style>
    * {
        margin:0;
        padding:0;
        /*font-family: 'Lobster', cursive;*/
        /*font-family: 'Open Sans Condensed', sans-serif;*/
        font-family: 'Roboto Condensed', sans-serif;
    }
    body {
        width: {{ $image->width }}px;
        height: {{ $image->height }}px;
        background: url({{ $image->image_url }}) 0px 0px no-repeat;
    }
    .text {
        position: absolute;
        /*vertical*/
        font-size: 3em;

        /*horizontal*/
        font-size: 3em;
        color: white;
        width: {{ $text_width }}px;
        margin: {{ $text_height }}px {{ ($image->width - $text_width) / 2 }}px 0;
        text-align: center;
        text-shadow: 0 0 10px black;
    }
    .text.text_shadow_1 {
        color: black;
        margin: {{ ($text_height + 1) }}px {{ (($image->width - $text_width) / 2) + 1  }}px 0;
    }
    .text.text_shadow_2 {
        color: black;
        margin: {{ ($text_height + 1) }}px {{ (($image->width - $text_width) / 2) - 1  }}px 0;
    }
    .text.text_shadow_3 {
        color: black;
        margin: {{ ($text_height - 1) }}px {{ (($image->width - $text_width) / 2) + 1  }}px 0;
    }
    .text.text_shadow_4 {
        color: black;
        margin: {{ ($text_height - 1) }}px {{ (($image->width - $text_width) / 2) - 1  }}px 0;
    }
</style>
<div class="container">
    <div class="text text_shadow_1">
        {!! $text !!}
    </div>
    <div class="text text_shadow_2">
        {!! $text !!}
    </div>
    <div class="text text_shadow_3">
        {!! $text !!}
    </div>
    <div class="text text_shadow_4">
        {!! $text !!}
    </div>
    <div class="text">
        {!! $text !!}
    </div>
</div>
</body>
</html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!--    <link href='http://fonts.googleapis.com/css?family=Scada&subset=latin,cyrillic' rel='stylesheet' type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&subset=latin,cyrillic' rel='stylesheet'-->
<!--          type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic,cyrillic-ext'-->
<!--          rel='stylesheet' type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed&subset=cyrillic' rel='stylesheet'-->
<!--          type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=Bad+Script&subset=latin,cyrillic' rel='stylesheet'-->
<!--          type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&subset=latin,cyrillic' rel='stylesheet'-->
<!--          type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=Andika&subset=latin,cyrillic' rel='stylesheet' type='text/css'>-->
</head>
<body>
<style>

/* Fonts */

* {
    /*font-family: 'rushinregular';*/
    margin: 0;
    padding: 0;
}

@font-face {
    font-family: 'PT Sans Narrow';
    src: url('/fonts/PT_Sans-Narrow-Web-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'Lobster';
    src: url('/fonts/Lobster-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'Ubuntu Condensed';
    src: url('/fonts/UbuntuCondensed-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'Bad Script';
    src: url('/fonts/BadScript-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}

/* Main */
body {
    background: #fff;
    /*height: 800px;*/
    position: relative;
    /*width: 1280px;*/
    width: {{ $image->width }}px;
    height: {{ $image->height }}px;
}

/* Image */
.image {
    /*height: 800px;*/
    /*width: 1280px;*/
    width: {{ $image->width }}px;
    height: {{ $image->height }}px;
    z-index: 1;
}


/* Image effects */

/*
 How to use: add this html after img tag
<i class="shadow-frame"></i>
*/

.hipster-frame {
    content: "";
    position: absolute;
    top: {{ $frame_margin }}px;
    left: {{ $frame_margin }}px;
    width: {{ ($image->width - 2 * $frame_margin - 20) }}px;
    height: {{ ($image->height - 2 * $frame_margin - 20) }}px;
    z-index: 3;
    border: 10px solid white;
    border: 10px solid rgba(255, 255, 255, .5);
    display: {{ $frame }};
}

.shadow-frame {
    box-shadow: inset 0 0 60px #000;
    content: "";
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 2;
}

.blur {
    filter: blur(2px);
}
.grayscale {
    -webkit-filter: grayscale(1);
    filter: grayscale(1);
}
.saturate {
    filter: saturate(150%);
}

.ig-xpro2 {
    -webkit-filter: contrast(1.3) brightness(0.8) sepia(0.3) saturate(1.5) hue-rotate(-20deg);
    filter: contrast(1.3) brightness(0.8) sepia(0.3) saturate(1.5) hue-rotate(-20deg);
}

.ig-walden {
    -webkit-filter: sepia(0.35) contrast(0.9) brightness(1.1) hue-rotate(-10deg) saturate(1.5);
    filter: sepia(0.35) contrast(0.9) brightness(1.1) hue-rotate(-10deg) saturate(1.5);
}

.ig-valencia {
    -webkit-filter: sepia(0.15) saturate(1.5) contrast(0.9);
    filter: sepia(0.15) saturate(1.5) contrast(0.9);
}

.ig-toaster {
    -webkit-filter: sepia(0.4) saturate(2.5) hue-rotate(-30deg) contrast(0.67);
    -filter: sepia(0.4) saturate(2.5) hue-rotate(-30deg) contrast(0.67);
}

.ig-rise {
    -webkit-filter: saturate(1.4) sepia(0.25) hue-rotate(-15deg) contrast(0.8) brightness(1.1);
    filter: saturate(1.4) sepia(0.25) hue-rotate(-15deg) contrast(0.8) brightness(1.1);
}

.ig-nashville {
    -webkit-filter: sepia(0.4) saturate(1.5) contrast(0.9) brightness(1.1) hue-rotate(-15deg);
    filter: sepia(0.4) saturate(1.5) contrast(0.9) brightness(1.1) hue-rotate(-15deg);
}

.ig-mayfair {
    -webkit-filter: saturate(1.4) contrast(1.1);
    filter: saturate(1.4) contrast(1.1);
}

.ig-lofi {
    filter: contrast(1.4) brightness(0.9) sepia(0.05);
    -webkit-filter: contrast(1.4) brightness(0.9) sepia(0.05);
}

.ig-hudson {
    -webkit-filter: contrast(1.2) brightness(0.9) hue-rotate(-10deg);
    filter: contrast(1.2) brightness(0.9) hue-rotate(-10deg);
}

.ig-amaro {
    -webkit-filter: hue-rotate(-10deg) contrast(0.9) brightness(1.1) saturate(1.5);
    filter: hue-rotate(-10deg) contrast(0.9) brightness(1.1) saturate(1.5);
}

.ig-1977 {
    -webkit-filter: sepia(0.5) hue-rotate(-30deg) saturate(1.2) contrast(0.8);
    filter: sepia(0.5) hue-rotate(-30deg) saturate(1.2) contrast(0.8);
}


/* Text styles */
.text {
    color: #fff;
    font-size: {{ $font_size }};
    font-family: {!! $font !!};
    position: absolute;
    width: {{ $text_width }}px;
    margin: -{{ $text_height }}px {{ ($image->width - $text_width) / 2 }}px 0;
    text-align: center;
    text-shadow: 0 0 10px black;
    z-index: 5;
}
.text.text_shadow_1 {
    color: black;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height + 2) }}px {{ (($image->width - $text_width) / 2) + 2  }}px 0;
}
.text.text_shadow_2 {
    color: black;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height + 2) }}px {{ (($image->width - $text_width) / 2) - 2  }}px 0;
}
.text.text_shadow_3 {
    color: black;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height - 2) }}px {{ (($image->width - $text_width) / 2) + 2  }}px 0;
}
.text.text_shadow_4 {
    color: black;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height - 2) }}px {{ (($image->width - $text_width) / 2) - 2  }}px 0;
}

/* Text styles */
.text.invert {
    color: #000;
    text-shadow: 0 0 10px white;
}
.text.text_shadow_1_invert {
    color: white;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height + 2) }}px {{ (($image->width - $text_width) / 2) + 2  }}px 0;
}
.text.text_shadow_2_invert {
    color: white;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height + 2) }}px {{ (($image->width - $text_width) / 2) - 2  }}px 0;
}
.text.text_shadow_3_invert {
    color: white;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height - 2) }}px {{ (($image->width - $text_width) / 2) + 2  }}px 0;
}
.text.text_shadow_4_invert {
    color: white;
    width: {{ $text_width }}px;
    margin: -{{ ($text_height - 2) }}px {{ (($image->width - $text_width) / 2) - 2  }}px 0;
}

/*
 How to use: add css class .text-black-shadow or .text-white-shadow
 to text block:
 <div class="text text-black-shadow">
    Если счастье до сих пор не пришло к вам, значит оно большое и идет к вам маленькими шагами!
</div>
*/

.text-black-shadow {
    background: radial-gradient(ellipse at center, rgba(0, 0, 0, 0.65) 0%, rgba(0, 0, 0, 0) 70%); /* W3C */
}

.text-white-shadow {
    background: radial-gradient(ellipse at center, rgba(255, 255, 255, 0.30) 0%, rgba(255, 255, 255, 0) 70%); /* W3C */
}

.text-black-shadow-full {
    background: radial-gradient(ellipse at center, rgba(0, 0, 0, 0.65) 0%, rgba(0, 0, 0, 0) 100%); /* W3C */
}

.text-white-shadow-full {
    background: radial-gradient(ellipse at center, rgba(255, 255, 255, 0.30) 0%, rgba(255, 255, 255, 0) 100%); /* W3C */
}

.text-position-top {
    top: 0;
    margin-top: 0;
    padding-top: 20%;
}

.text-position-top.text-black-shadow {
    background: linear-gradient(to bottom, rgba(0, 0, 0, 0.65) 0%, rgba(0, 0, 0, 0) 100%); /* W3C */
}

.text-position-bottom {
    bottom: 0;
    top: auto;
    padding-bottom: 10%;
    margin-top: 0;
}

/*.text-position-bottom.text-black-shadow {*/
    /*background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.65) 100%); *//* W3C */
/*}*/

/* Fonts */

.font-roboto-cond {
    font-family: 'Roboto Condensed', sans-serif;
}

.font-bad-script {
    font-family: 'Bad Script', cursive;
    font-weight: bold;
    text-transform: uppercase;
}

.font-lobster {
    font-family: 'Lobster', cursive;
    letter-spacing: 2px;
}

.font-ubuntu-cond {
    font-family: 'Ubuntu Condensed', sans-serif;
    text-transform: lowercase;
}

.text-ptsans-narrow {
    font-family: 'PT Sans Narrow', sans-serif;
}

/* Frame styles */
.frame-top {
    height: 15%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 2;
}

.frame-bottom {
    bottom: 0;
    height: 15%;
    left: 0;
    position: absolute;
    width: 100%;
    z-index: 2;
}

.watermark {
    opacity: 0.7;
    font-weight: bold;
    position: absolute;
    z-index: 9000;
    font-size: 1em;
}
.watermark-text {
    bottom: 10px;
    right: 20px;
    color: #ffffff;
    z-index: 9001;
}
.watermark-shadow {
    bottom: 8px;
    right: 18px;
    color: #000;
}

</style>

<img class="image blur {{ $filter }}" src="{{ $image->image_url }}"/>
<div class="text text_shadow_1">
    {!! $text !!}
</div>
<div class="text text_shadow_2">
    {!! $text !!}
</div>
<div class="text text_shadow_3">
    {!! $text !!}
</div>
<div class="text text_shadow_4">
    {!! $text !!}
</div>
<div class="text">
    {!! $text !!}
</div>
<i class="shadow-frame"></i>
<i class="hipster-frame"></i>
<i class="frame-top frame-style-1"></i>
<i class="frame-bottom frame-style-1"></i>
<div class="watermark watermark-text text-ptsans-narrow">Гуру цитат</div>
<div class="watermark watermark-shadow text-ptsans-narrow">Гуру цитат</div>
</body>
</html>
@extends('app')

@section('content')
    <h1>Редактирование открытки</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::model($card, ['method' => 'PATCH', 'action' => ['CardController@update', $card->id]]) !!}
        <input type="hidden" name="page" value="<?=$_GET['page']?>">
        <table border="0" cellspacing="1" cellpadding="1">
            <tr>
                <td>
                    <div class="form-group">
                        <img src="{{ $card->src_big }}"/>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::label('tags[]', 'Теги') !!}
                        {!! Form::select(
                                'tags[]',
                                $tags,
                                array_keys($card->getTags()),
                                [
                                    'class' => 'form-control',
                                    'multiple' => 'multiple',
                                    'style' => 'height: 300px;'
                                ]
                            )
                        !!}
                    </div>
                </td>
            </tr>
        </table>
        <div class="form-group">
            {!! Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    {!! Form::close() !!}
@stop
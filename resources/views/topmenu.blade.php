<ul class="nav navbar-nav">
    @if (!Auth::guest())
    <li><a href="{{ url('tumblr') }}">Tumblr</a></li>
    <li><a href="{{ url('twitter') }}">Twitter</a></li>
    <li><a href="{{ url('tag') }}">Теги</a></li>
    <li><a href="{{ url('image') }}">Картинки</a></li>
    <li><a href="{{ url('quote') }}">Цитаты</a></li>
    <li class="dropdown">
        <a href="{{ url('card') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Открытки <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ url('card/queue') }}">Очередь</a></li>
            <li><a href="{{ url('card/create') }}">Создать</a></li>
        </ul>
    </li>
    <li><a href="/statistic">Статистика</a></li>
    @endif
    <li><a href="/allcards">Все цитаты</a></li>
    <li><a href="/tagcloud">Список тегов</a></li>
    <li><a href="/about">О сайте</a></li>
</ul>
@extends('app')

@if (!$title)
@section('title', Config::get('app.title'))
@else
@section('title', 'Гуру цитат: ' . $title)
@endif


@section('content')
    <div class="row">
        <div class="main-page-column">
            <ul class="list-group">
                <li class="list-group-item">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Баннер в цитаты -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:468px;height:60px"
                         data-ad-client="ca-pub-3071115311588872"
                         data-ad-slot="1624883540"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </li>
                <li class="list-group-item list-group-item-info">
                    <div class="btn-group" role="group" aria-label="...">
                        <a href="/view/{{ $card->getPrev() }}" alt="Предыдущая открытка с цитатой" title="Предыдущая открытка с цитатой">
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-arrow-left"></span>
                            </button>
                        </a>
                        <a href="/random" alt="Случайная открытка с цитатой" title="Случайная открытка с цитатой">
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-repeat"></span>
                            </button>
                        </a>
                        <a href="/view/{{ $card->getNext() }}" alt="Следующая открытка с цитатой" title="Следующая открытка с цитатой">
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-arrow-right"></span>
                            </button>
                        </a>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="/view/{{ $card->getNext() }}" alt="Следующая открытка с цитатой" title="Следующая открытка с цитатой">
                        <img src="{{ $card->src_big }}" alt="Крылатая фраза: {{ $quote }}" title="Крылатая фраза: {{ $quote }}"/>
                    </a>
                </li>
                @if (count($card->getTags()))
                    <li class="list-group-item list-group-item-info">
                        @foreach($card->getTags() as $tag_id => $tag)
                            <a href="/tag/{{ $tag_id }}">#{{ $tag }}</a>&nbsp;&nbsp;
                        @endforeach
                    </li>
                @endif
                <li class="list-group-item">
                    <table class="social-buttons-detail-card">
                        <tr>
                            <td>
                                <!----------------------------VK------------------------->
                                <script type="text/javascript">
                                    VK.init({apiId: 4870957, onlyWidgets: true});
                                </script>
                                <div id="vk_like" class="col-xs-1"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Like(
                                        "vk_like",
                                        {
                                            type: "mini",
                                            pageUrl: "{!! $url !!}",
                                            pageImage: "{{ $card->src_big }}",
                                            text: "{{ str_replace(['-', ' ', ':'], '', $card->getQuote()) }}"
                                        }
                                    );
                                </script>
                                <!----------------------------VK------------------------->
                            </td>
                            <td>
                                <!----------------------------FB------------------------->
                                <div class="fb-like col-xs-1" data-href="{!! $url !!}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                                <!----------------------------FB------------------------->
                            </td>
                            <td>
                                <!-------------------------Twitter----------------------->
                                <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru"
                                   data-hashtags="гуруцитат@if (count($card->getTags()))
                                    @foreach($card->getTags() as $tag_id => $tag) ,{{ $tag }} @endforeach
                                    @endif"
                                >Твитнуть</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                <!-------------------------Twitter----------------------->
                            </td>
                        </tr>
                    </table>
                </li>
            </ul>
        </div>
    </div>
@stop
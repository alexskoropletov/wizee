@extends('app')
@section('title', 'Гуру цитат: о сайте')
@section('content')
<div class="row">
    <div class="main-page-column">
        <h1>О сайте</h1>
        <p>«Гуру цитат» основан в 2015-м году.</p>
        <p>
            Цель проекта — собрать в одном месте <b>высказывания</b> и <b>крылатые фразы</b>, несущие в себе <a href="/tag/8">#мудрость</a> веков.
            Краткие и лаконичные они могут помочь в сложной ситуации, дать совет, <a href="/tag/9">#мотивировать</a> на поступок который, возможно, изменит всю Вашу <a href="/tag/10">#жизнь</a>.
            В них отражено всё самое желанное в жизни человека: <a href="/tag/1">#счастье</a>, <a href="/tag/2">#любовь</a>, <a href="/tag/16">#путешествия</a>, <a href="/tag/19">#богатсво</a> и <a href="/tagcloud">многое другое</a>.
        </p>
        <p>
            Благодаря современным технологиям афоризмы и крылатые фразы на сайте представлены в красивой форме открытки с интересным фоном. Не стесняйтесь делиться ими с друзьями в социальных сетях ;)
        </p>
    </div>
</div>
@stop
@extends('app')

@if (isset($tag))
@section('title', 'Гуру цитат: все цитаты по тегу "' . $tag->title . '"')
@else
@section('title', 'Гуру цитат: все цитаты')
@endif

@section('content')
    <script type="text/javascript">
        VK.init({apiId: 4870957, onlyWidgets: true});
    </script>
    <div class="row">
        <div class="main-page-column">
            @if (isset($tag))
                <h1>Цитаты по тегу #{{ $tag->title }}</h1>
            @endif
            {!! $cards->render() !!}
            @foreach($cards as $key => $card)
                <ul class="list-group">
                    <li class="list-group-item list-group-item-info">
                        <a href="/view/{{ $card->id }}">Цитата <b>{{ $card->id }}</b></a>
                    </li>
                    <li class="list-group-item">
                        <a href="/view/{{ $card->id }}" style="vertical-align: middle;">
                            <img src="{{ $card->src_big }}" alt="Гуру цитат: {{ $card->getQuote() }}" title="Гуру цитат: {{ $card->getQuote() }}">
                        </a>
                    </li>
                    @if (count($card->getTags()))
                        <li class="list-group-item list-group-item-info">
                            @foreach($card->getTags() as $tag_id => $tag)
                            <a href="/tag/{{ $tag_id }}">#{{ $tag }}</a>&nbsp;&nbsp;
                            @endforeach
                        </li>
                    @endif
                    <li class="list-group-item">
                        <table class="social-buttons-detail-card">
                            <tr>
                                <td>
                                    <!----------------------------VK------------------------->
                                    <div id="vk_like{{ $card->id }}" class="col-xs-1"></div>
                                    <script type="text/javascript">
                                        VK.Widgets.Like(
                                            "vk_like{{ $card->id }}",
                                            {
                                                type: "mini",
                                                pageUrl: "{{ url('/view/' . $card->id) }}",
                                                pageImage: "{{ $card->src_big }}",
                                                text: "{{ str_replace(['-', ' ', ':'], '', $card->getQuote()) }}"
                                            }
                                        );
                                    </script>
                                    <!----------------------------VK------------------------->
                                </td>
                                <td>
                                    <!----------------------------FB------------------------->
                                    <div class="fb-like col-xs-1" data-href="{{ url('/view/' . $card->id) }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                                    <!----------------------------FB------------------------->
                                </td>
                                <td>
                                    <!-------------------------Twitter----------------------->
                                    <a href="https://twitter.com/share"
                                       class="twitter-share-button"
                                       data-lang="ru"
                                       data-url="{{ url('/view/' . $card->id) }}"
                                       data-hashtags="гуруцитат@if (count($card->getTags()))
                                        @foreach($card->getTags() as $tag_id => $tag) ,{{ $tag }} @endforeach
                                        @endif"
                                    >Твитнуть</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                    <!-------------------------Twitter----------------------->
                                </td>
                            </tr>
                        </table>
                    </li>
                </ul>
            @endforeach
            {!! $cards->render() !!}
        </div>
    </div>
@stop
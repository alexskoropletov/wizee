<div class="row">
    @foreach($cards as $card)
        <div class="col-xs-6 col-sm-2" style="height: 153px; vertical-align: middle;">
            <div class="panel {{ $card->getPanel() }}">
                <div class="panel-heading">
                    <a href="/view/{{ $card->id }}">Цитата <b>{{ $card->id }}</b></a>
                </div>
                <div class="panel-body">
                    <center>
                    <a href="/view/{{ $card->id }}" style="vertical-align: middle;">
                        <img src="{{ $card->src }}" alt="Гуру цитат: {{ $card->getQuote() }}" title="Гуру цитат: {{ $card->getQuote() }}">
                    </a>
                    </center>
                </div>
            </div>
        </div>
    @endforeach
</div>

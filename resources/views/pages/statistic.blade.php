@extends('app')

@section('content')
    <div class="row">
        <table class="table">
            <caption><h4>Цитаты</h4></caption>
            <thead>
                <tr>
                    <th>Всего</th>
                    <th>Одобрено</th>
                    <th>Использовано</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $quotes->count() }}</td>
                    <td>{{ $quotes->filter(function($quote) {return $quote->approved;})->count() }}</td>
                    <td>{{ $quotes->filter(function($quote) {return $quote->used;})->count() }}</td>
                </tr>
            </tbody>
        </table>
        <table class="table">
            <caption><h4>Картинки</h4></caption>
            <thead>
                <tr>
                    <th>Всего</th>
                    <th>Одобрено</th>
                    <th>Использовано</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $images->count() }}</td>
                    <td>{{ $images->filter(function($image) {return $image->approved;})->count() }}</td>
                    <td>{{ $images->filter(function($image) {return $image->used;})->count() }}</td>
                </tr>
            </tbody>
        </table>
        <table class="table">
            <caption><h4>Открытки</h4></caption>
            <thead>
                <tr>
                    <th>Создано</th>
                    <th>Опубликовано</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $cards->count() }}</td>
                    <td>{{ $cards->filter(function($card) {return $card->published;})->count() }}</td>
                </tr>
            </tbody>
        </table>
    </div>
@stop
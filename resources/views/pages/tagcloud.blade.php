@extends('app')
@section('title', 'Гуру цитат: список тегов')
@section('content')
<div class="row">
    @foreach($tags as $tag)
    <div class="col-xs-6 col-sm-2" style="vertical-align: middle;">
        <div class="panel">
            <div class="panel-body">
                <center>
                    <a href="/tag/{{ $tag->id }}" style="vertical-align: middle;">
                        {{ $tag->title }} <span class="badge">{{ $tag->getCount() }}</span>
                    </a>
                </center>
            </div>
        </div>
    </div>
    @endforeach
</div>
@stop
@extends('app')

@section('content')
    <script type="text/javascript">
        VK.init({apiId: 4870957, onlyWidgets: true});
    </script>
    <div class="row">
        <ol class="breadcrumb">
            <li class="active">
                <h4>Цитата дня</h4>
            </li>
        </ol>
    </div>
    <div class="row oftheday">
        <ul class="list-group">
            <li class="list-group-item">
                <img src="{{ $oftheday->src_big }}" alt="Крылатая фраза: {{ $quote }}"
                     title="Крылатая фраза: {{ $quote }}"/>
            </li>
            @if (count($oftheday->getTags()))
                <li class="list-group-item list-group-item-info">
                    @foreach($oftheday->getTags() as $tag_id => $tag)
                        <a href="/tag/{{ $tag_id }}">#{{ $tag }}</a>&nbsp;&nbsp;
                    @endforeach
                </li>
            @endif
        </ul>
    </div>
    <div class="row">
        <ol class="breadcrumb">
            <li class="active">
                <h4>Самые свежие</h4>
            </li>
        </ol>
    </div>
    <div class="row">
        @foreach($latest as $key => $card)
            <div class="col-md-6">
                <a href="/view/{{ $card->id }}">
                    <img src="{{ $card->src_big }}" alt="{{ $card->getQuote() }}" class="img-thumbnail">
                </a>
            </div>
        @endforeach
    </div>
    <div class="row">
        &nbsp;
    </div>
    <div class="row">
        <ol class="breadcrumb">
            <li class="active">
                <h4>Цитаты без картинок</h4>
            </li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item list-group-item-info">
                    По тегу <a href="/tag/11">#юмор</a>
                </li>
                @foreach($nopicture['humor'] as $quote)
                    <li class="list-group-item">
                        {{ strip_tags($quote->title) }}
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item list-group-item-info">
                    По тегу <a href="/tag/8">#мудрость</a>
                </li>
                @foreach($nopicture['wisdom'] as $quote)
                    <li class="list-group-item">
                        {{ strip_tags($quote->title) }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item list-group-item-info">
                    По тегу <a href="/tag/2">#любовь</a>
                </li>
                @foreach($nopicture['love'] as $quote)
                    <li class="list-group-item">
                        {{ strip_tags($quote->title) }}
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item list-group-item-info">
                    По тегу <a href="/tag/9">#мотивация</a>
                </li>
                @foreach($nopicture['motivation'] as $quote)
                    <li class="list-group-item">
                        {{ strip_tags($quote->title) }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
@extends('app')

@section('content')
    <h1>Редактирование источника Tumblr</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::model($tumblr, ['method' => 'PATCH', 'action' => ['TumblrController@update', $tumblr->id]]) !!}
        @include ('tumblr.form')
    {!! Form::close() !!}
@stop
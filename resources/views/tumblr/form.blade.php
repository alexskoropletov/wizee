<div class="form-group">
    {!! Form::label('source_url', 'Источник') !!}
    {!! Form::text('source_url', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('latest', 'Номер последнего поста') !!}
    {!! Form::text('latest', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('tags[]', 'Теги') !!}
    {!! Form::select('tags[]', $tags, array_keys($tumblr->getTags()), ['class' => 'form-control', 'multiple' => 'multiple']) !!}
</div>
<div class="form-group">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) !!}
</div>
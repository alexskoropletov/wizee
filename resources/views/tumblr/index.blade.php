@extends('app')

@section('content')
    <h1>Список источников Tumblr</h1>
    <hr/>
    <a href="tumblr/create">Добавить</a>
    <hr/>
    <table class="table table-bordered">
        @foreach($tumblrs as $tumblr)
            <tr>
                <th scope="row">
                    {{ $tumblr->id }}
                </th>
                <td>
                    <a href="/tumblr/{{ $tumblr->id }}/edit">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>

                    <a href="/tumblr/{{ $tumblr->id }}/destroy">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
                <td><a href="{{ $tumblr->source_url }}" target="_blank">{{ $tumblr->source_url }}</a></td>
                <td>{{ implode(", ", $tumblr->getTags()) }}</td>
            </tr>
        @endforeach
    </table>
@stop
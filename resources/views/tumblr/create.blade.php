@extends('app')

@section('content')
    <h1>Добавить источник в Tumblr</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::open(['url' => 'tumblr']) !!}
        @include ('tumblr.form')
    {!! Form::close() !!}
@stop
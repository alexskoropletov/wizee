@extends('app')

@section('content')
    <h1>Список источников Twitter</h1>
    <hr/>
    <a href="twitter/create">Добавить</a>
    <hr/>
    <table class="table table-bordered">
        @foreach($twitters as $twitter)
            <tr>
                <th scope="row">
                    {{ $twitter->id }}
                </th>
                <td>
                    <a href="/twitter/{{ $twitter->id }}/edit">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <a href="/twitter/{{ $twitter->id }}/destroy">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
                <td><a href="{{ $twitter->source_url }}" target="_blank">{{ $twitter->source_url }}</a></td>
                <td>{{ implode(", ", $twitter->getTags()) }}</td>
            </tr>
        @endforeach
    </table>
@stop
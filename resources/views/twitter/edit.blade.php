@extends('app')

@section('content')
    <h1>Редактирование источника Twitter</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::model($twitter, ['method' => 'PATCH', 'action' => ['TwitterController@update', $twitter->id]]) !!}
        @include ('twitter.form')
    {!! Form::close() !!}
@stop
@extends('app')

@section('content')
    <h1>Добавить источник в Twitter</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::open(['url' => 'twitter']) !!}
        @include ('twitter.form')
    {!! Form::close() !!}
@stop
{!! Form::open(['method' => 'GET', 'url' => 'quote']) !!}
    <div class="form-group">
        {!! Form::label('approved', 'Одобренные') !!}
        {!! Form::checkbox('approved', 1, ($approved == 1)) !!}
        &nbsp;&nbsp;&nbsp;
        {!! Form::label('used', 'Опубликованые') !!}
        {!! Form::checkbox('used', 1, ($used == 1)) !!}
        &nbsp;&nbsp;&nbsp;
        {!! Form::label('used', 'Сортировка') !!}
        {!! Form::select('order', ['id' => 'id', 'date' => 'дате', 'ratio' => 'размеру'], $order, ['class' => 'form-control']) !!}
        &nbsp;&nbsp;&nbsp;
        {!! Form::label('twitters[]', 'Источники') !!}
        {!! Form::select('twitters[]', $twitters, $selected_twitters, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Показать', ['class' => 'btn btn-primary form-control']) !!}
    </div>
{!! Form::close() !!}
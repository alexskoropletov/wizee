@extends('app')

@section('content')
    <h1>Редактирование цитаты</h1>
    <hr/>
    @include ('errors.list')
    {!! Form::model($quote, ['method' => 'PATCH', 'action' => ['QuoteController@update', $quote->id]]) !!}
        <input type="hidden" name="page" value="<?=$_GET['page']?>">
        <input type="hidden" name="approved_pager" value="<?=$_GET['approved']?>">
        <input type="hidden" name="used_pager" value="<?=$_GET['used']?>">
        <input type="hidden" name="order_pager" value="<?=$_GET['order']?>">
        @foreach($selected_twitters as $selected)
            <input type="hidden" name="twitters[]" value="{{ $selected }}">
        @endforeach
        @include ('quote.form')
    {!! Form::close() !!}
@stop
@extends('app')

@section('content')
    <h1>Список цитат</h1>
    <div>
        <table width="100%">
            <tr>
                <td>
                    {!! $quotes->appends(['approved' => $approved, 'used' => $used, 'order' => $order, 'twitters' => $selected_twitters])->render() !!}
                </td>
                <td align="right">
                    <a href="#" id="show_list_index_filter">Показать фильтр</a>
                </td>
            </tr>
        </table>
        <div id="list_index_filter" style="display: none;">
            @include ('quote.filter', ['approved' => $approved, 'selected_twitters' => $selected_twitters, 'twitters' => $twitters, 'used' => $used, 'order' => $order])
        </div>
    </div>
    <table class="table table-bordered">
        @foreach($quotes as $quote)
            <tr id="quote{{ $quote->id }}" @if (!$quote->approved) class="danger" @endif @if ($quote->used) class="success" @endif>
                <td scope="row">
                    <b>{{ $quote->id }}</b>
                </td>
                <td>
                    <div class="btn-toolbar" role="toolbar" aria-label="Tools">
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/quote/{{ $quote->id }}/edit?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}@foreach($selected_twitters as $selected)&twitters[]={{ $selected }}@endforeach">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/quote/{{ $quote->id }}/approve?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}@foreach($selected_twitters as $selected)&twitters[]={{ $selected }}@endforeach" alt="Одобрить">
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/quote/{{ $quote->id }}/unused?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}@foreach($selected_twitters as $selected)&twitters[]={{ $selected }}@endforeach" alt="Не использовано">
                                <span class="glyphicon glyphicon-ok"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/quote/{{ $quote->id }}/disable?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}@foreach($selected_twitters as $selected)&twitters[]={{ $selected }}@endforeach" alt="Не показывать никогда">
                                <span class="glyphicon glyphicon-minus"></span>
                            </a>
                        </div>
                        <div class="btn-group" role="group" aria-label="Tools">
                            <a href="/quote/{{ $quote->id }}/destroy?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}@foreach($selected_twitters as $selected)&twitters[]={{ $selected }}@endforeach">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </div>
                    </div>
                </td>
                <td><a href="/quote/{{ $quote->id }}">{{ $quote->title }}</a></td>
                <td>
                    @if (isset($quote->source))
                        <a href="/quote/?page={{ $page }}&approved={{ $approved }}&used={{ $used }}&order={{ $order }}&twitters[]={{ array_flip($twitters)[$quote->source] }}@foreach($selected_twitters as $selected)&twitters={{ $selected }}@endforeach">
                            {{ $quote->source }}
                        </a>
                    @endif
                </td>
                <td>{{ implode(", ", $quote->getTagsBySource()) }}</td>
            </tr>
        @endforeach
    </table>
    {!! $quotes->appends(['approved' => $approved, 'used' => $used, 'order' => $order, 'twitters' => $selected_twitters])->render() !!}
@stop
<div class="form-group">
    {!! Form::label('approved', 'Публиковать') !!}
    {!! Form::checkbox('approved', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'Цитата') !!}
    {!! Form::textarea('title', null, ['class' => 'form-control']) !!}
</div>
<!--<div class="form-group">-->
<!--    {!! Form::label('source', 'Источник') !!}-->
<!--    {!! Form::text('source', null, ['class' => 'form-control']) !!}-->
<!--</div>-->
<!--<div class="form-group">-->
<!--    {!! Form::label('published_at', 'Опубликована') !!}-->
    {!! Form::input('hidden', 'published_at', date("Y-m-d H:i:s"), ['class' => 'form-control']) !!}
<!--</div>-->
<div class="form-group">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) !!}
</div>
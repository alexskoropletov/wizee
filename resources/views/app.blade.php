<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title', Config::get('app.title'))</title>

    <!-- for Google -->
    <meta name="description" content="Цитаты, афоризмы, крылатые выражения, картинки и мудрые мысли - все в одном от Гуру Цитат." />
    <meta name="keywords" content="афоризмы великих людей, прикольные афоризмы, афоризмы, крылатые фразы, смешные цитаты, мудрые цитаты, высказывания, афоризмы со смыслом, крылатые выражения, мудрые афоризмы" />
    <meta name="author" content="Гуру цитат" />
    <meta name="copyright" content="Гуру цитат" />
    <meta name="application-name" content="Гуру цитат" />

    <!-- for Facebook -->
    <meta property="og:title" content="@yield('title', Config::get('app.title'))" />
    <meta property="og:type" content="article" />

    <!-- for Twitter -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="@yield('title', Config::get('app.title'))" />

    @if (isset($card))
    <link rel="image_src" href="{{ $card->src_big }}"/>
    <!-- for Facebook -->
    <meta property="og:image:secure_url" content="{{ $card->src_big }}" />
    <meta property="fb:app_id" content="1651590208410249" />
    <meta property="og:description" content="@yield('title', Config::get('app.title')) @if (count($card->getTags())) @foreach($card->getTags() as $tag_id => $tag) #{{ $tag }} @endforeach @endif" />
    <meta property="og:url" content="http://citata.guru/view/{{ $card->id }}" />
    <!-- for Twitter -->
    <meta name="twitter:description" content="@yield('title', Config::get('app.title')) @if (count($card->getTags())) @foreach($card->getTags() as $tag_id => $tag) #{{ $tag }} @endforeach @endif" />
    <meta name="twitter:image" content="{{ $card->src_big }}" />
    @endif

	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-media-lightbox.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <!-- Put this script tag to the <head> of your page -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.3&appId=1651590208410249";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
                    <a class="navbar-brand" href="/">Гуру цитат <span class="badge">найди свой смысл</span></a>
			</div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                @include ('topmenu')
                @if (!Auth::guest())
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
		</div>
	</nav>

    <div class="container">
	    @yield('content')
    </div>

    <footer class="footer">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="https://vk.com/gurucitat" target="_blank" alt="Наша группа в VK" title="Наша группа в VK">
                                <img src="/zvety-v-vk.jpg" alt="Наша группа в VK" title="Наша группа в VK" width="23" height="23">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/gurucitat" target="_blank" alt="Наша страница в Facebook" title="Наша страница в Facebook">
                                <img src="/Facebook_logo_jpg-5.png" alt="Наша страница в Facebook" title="Наша страница в Facebook" width="16" height="16">
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="http://skoropletov.ru" target="_blank">&copy http://skoropletov.ru, 2015</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </footer>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/bootstrap-media-lightbox.js') }}"></script>
    <script src="{{ asset('js/card.js') }}"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-61886804-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter30603842 = new Ya.Metrika({id:30603842,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/30603842" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</body>
</html>